# Beep Boop
That's all my dotfiles. 
Also i'm not consistent in using things properly, so some scripts and configs may try to find something on non-existing user, so it's necessary to check through the code for directories and dependencies

## documents/scripts
There placed almost all my scripts that i use

### addtogit.sh
Template for future
### edit-config.sh
Opens menu, where i can choose config i want to edit
### gitupdate
Updates git
### lockscreen.sh
Locks screen with your current workspace as blured background (i3lock)
### powermenu.sh
Powermenu with rofi
### reorganize_gnome_apps.py
Organize apps in gnome menu in alphabetical order. To use you need to provide ```tmp.txt``` file which contains current value of org.gnome.shell.app-picker-layout. After executing will drop ```output.txt``` which you should paste back to org.gnome.shell.app-picker-layout into custom value
### screenshot.sh
Automates xfce4-screenshoter
### systemupdate.sh
Full system update with clearing caches and orphanes
### tiling-grid.sh
Not ready at all
### tiling-master.sh
Tiling for wm, shows rofi's menu where you choose master window, other will be placed on the right side as grid

## .config

### Polybar:
The most interesting parts are in scripts folder:

Calendar - little calendar made with dunst

Hackspeed - shows your cymbols per minute

kdec - kde-connect for your bar. Shows your device's battery as icon color, click on it gives you rofi menu with ability to ping/send files/browse folder and battery percentage
idk why left-clicking not working, so you have to place action for right-click 

tray - spawns one more polybar only for tray(works fine for screenshots but not really usable because of huge latency)
killbar - small script for killing polybar tray manualy

trayer - spawns trayer on top of main bar

updates - shows number of available updates (showing them only if more than 20)

yes - spawns one more polybar with some info

## themes/Numix2
Theme for openbox, based on Numix, i use it only for coloring borders

## wallpapers
Moved [here](https://gitlab.com/k1ake/wallpapers 'Wallpapers')
