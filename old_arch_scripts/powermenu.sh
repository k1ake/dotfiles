#!/bin/bash
# icons main
shutdown="襤"
reboot="ﰇ"
lock=""
advanced=""
logout=""
# icons advanced
firmware=""
suspend="鈴"
restart=""
update=""
git=""
#variables
theme="powermenu.rasi"
updates=$(tail -n1 $HOME/documents/scripts/tmp/updates.txt)
if [ "$updates" -gt 0 ]; then
    upd="Updates: $updates"
else
    upd="System is up to date"
fi

#options to show
options_main="$lock\n$reboot\n$shutdown\n$logout\n$advanced"
options_advanced="$suspend\n$firmware\n$restart\n$update\n$git"

advanced() {
	advanced=$(echo -e $options_advanced | rofi \
		 -dmenu -p "$upd | $(date +%H:%M)" \
	 	 -theme $theme -selected-row 2)
	if [[ -z $advanced ]]
		then $(main)
	fi
	case $advanced in
		$update) 		alacritty -e $HOME/documents/scripts/systemupdate.sh;;
		$firmware) 		systemctl reboot --firmware-setup;;
		$git) 			alacritty -e $HOME/documents/scripts/gitupdate.sh;;
		$suspend) 		systemctl hibernate;;
		$restart) 		openbox --restart;;
	esac
}

main() {
main=$(echo -e $options_main | rofi \
	 -dmenu -p "$upd | $(date +%H:%M)" \
 	 -theme $theme -selected-row 2)
if [[ -z $main ]]
	then exit
fi
case $main in
	$shutdown) 		rm -rf $HOME/documents/scripts/tmp/snap.txt && poweroff;;
	$reboot) 		reboot;;
	$lock) 			$HOME/documents/scripts/lockscreen.sh;;
	$advanced) 		$(advanced);;
	$logout) 		openbox --exit;;
esac
}


$(main)
