#!/bin/bash
myphoto=$(mktemp)
mv "$(xfce4-screenshooter -fo ls)" $myphoto
convert $myphoto -blur 0x7 $myphoto

i3lock -i $myphoto \
	-k \
	--time-pos="ix-600:iy+350" \
	--time-color="bfbbaaff" \
	--time-font="Ubuntu" \
	--time-size=80 \
	--date-font="Nerd" \
	--date-str="%D" \
	--date-color="bfbbaaff"\
	--date-size=30 \
	--greeter-font="Anonymice Nerd Font" \
	--greeter-text="" \
	--greeter-size=60 \
	--greeter-color="bfbbaaff"\
	--greeter-pos="ix-4:iy-430" \
	--noinput-text="boop" \
	--wrong-color="292524ff" \
    --wrong-font="Ubuntu" \
	--wrong-text="Nah.." \
	--verif-text="Trying.." \
	--ring-color="BFBBAAff" \
	--inside-color="BFBBAA55" \
	--insidever-color="BFBBAA55" \
	--ringver-color="BFBBAAaa" \
	--ringwrong-color="963A2Aaa" \
	--insidewrong-color="BFBBAA55" \
	--keyhl-color="E2A155ff" \
