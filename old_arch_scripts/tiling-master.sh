#!/bin/bash
# Set your gaps here (2 is default)
padding=2 # That is percents
curdesk=$(wmctrl -d | awk '{if ($2=="*") print $1}') 

# Showing windows on current desktop and getting master window number
master=$(wmctrl -lx | awk -v cur="$curdesk" '$2==cur{sub(/.*\./,"",$3); print $3 ":",$5,$6,$7,$8,$9,$10}' | \
	nl -n ln | rofi -sep "\n" -dmenu -i -p 'Choose your master:' -theme tiling | \
	awk '{print $1}')

# Checking for choice completion
if [[ -z $master ]]
	then exit
fi

# Recieving Master window id
master_p=$(wmctrl -lp | awk -v cur="$curdesk" '$2==cur{print $1}' | \
	nl -n ln | awk -v num="$master" '$1==num{print $2}')


# Getting bar height and pos
# Pos1 > 0 if bar on top and Pos2 > 0 if bar on bottom
barH=$(xprop -name $(wmctrl -l | awk '{ if ($2 == -1) print $4}') "_NET_WM_STRUT" | \
 	awk '{print $5$6}' | sed 's/,/ /')

a=$(echo $barH | awk '{if ($1 > 0) print 1}')
if [[ a -gt 0 ]]
	then 
		bar_pos=1
		booba=$(echo $barH | awk '{ print $1 }' )
	else 
		bar_pos=0
		booba=$(echo $barH | awk '{ print $2 }' )
fi

if [[ -z $barH ]]
	then bar_pos=3; booba=0
fi


# Getting screen Geometry
geometry=$(wmctrl -d | awk '{if ($2=="*") print $4}' | sed 's/x/ /')
width=$(echo "$geometry" | awk '{print $1}')
height=$(echo "$geometry" | awk '{print $2}')
height=$(( $height - ( $booba / 2 ) ))

# Getting number of windows on current desktop and counting number of slaves
windows=$(wmctrl -lp | awk -v cur="$curdesk" '{if ($2==cur) print $5}' | wc -l)
slaves=$(($windows - 1))

# Padding
pad_w=$(( $width * $padding / 100 ))
pad_h=$(( $height * $padding / 100 ))
spad_h=$(( $pad_h * 2 ))

# Windows width, Master height, Slave Height, Slave x-pos
w=$((($width - (3 * $pad_w)) / 2 ))  
m_h=$(($height - (2 * $pad_h) - $pad_h )) 
s_h=$((($height - ($pad_h * $windows)) / $slaves - ($pad_h / $slaves ) ))
s_x=$((2*$pad_w + $w))


# Moving windows
cur_y=$(( $pad_h + $booba * $bar_pos ))
m_y=$(( $pad_h + $booba * $bar_pos ))
wmctrl -lp | awk -v cur="$curdesk" '{if ($2==cur) print $1}' | while read y
do
	if [[ $y == $master_p ]]
		then 
			wmctrl -ir $y -e 0,$pad_w,$m_y,$w,$m_h
	else
		wmctrl -ir $y -e 0,$s_x,$cur_y,$w,$s_h
		cur_y=$(($cur_y + $s_h + $pad_h))
	fi 
done
xdotool windowfocus $master_p



