#!/bin/bash

# Names of programs and their paths to config files
openbox="micro $HOME/.config/openbox/rc.xml"
polybar="ranger $HOME/.config/polybar"
dunst="micro $HOME/.config/dunst/dunstrc"
rofi="ranger $HOME/.config/rofi/"
fish="micro $HOME/.config/fish/config.fish"
scripts="ranger $HOME/documents/scripts/"

# Array with names of programs which config you want to edit
array=$(echo -e "openbox" "polybar" "scripts" "dunst" \
	 "fish" "rofi")
choose="$(echo $array | rofi -sep " " -dmenu -i -p "Choose config" -theme configs)"

if [[ -z $choose ]]
	then exit
fi

case "$choose" in
	*'openbox') alacritty -e $openbox;;
	*'polybar')alacritty -e $polybar;;
	*'dunst') alacritty -e $dunst;;
	*'rofi') alacritty -e $rofi;;
	*'fish') alacritty -e $fish;;
	*'scripts') alacritty -e  $scripts;;
esac

alacritty -e 
