#!/bin/bash
read -p "Enter password: " -s pass
echo -e "\n===============\nCleaning cache\n==============="
echo $pass | sudo -S paccache -rk1
echo -e "\n============\nArch Update\n============"
echo $pass | sudo -S pacman -Syuq --noconfirm
echo -e "\n===========\nAUR Update\n==========="
yay -Syu --nodiffmenu --noeditmenu --answerupgrade y --nocombinedupgrade
echo -e "\n=======================\nCleaning useless files\n======================="
echo $pass | sudo -S pacman -Rs $(pacman -Qtdq)
echo -e "\n===============\nCleaning cache\n==============="
paccache -ruk0
echo > /home/merzost/documents/updates.txt
read -p "Press Enter to exit ..."
