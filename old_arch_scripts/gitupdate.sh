#!/bin/bash
gits=$( ls $HOME/mygit/ )
chosen=$(echo -e $gits | rofi -dmenu -sep " ")
if [[ -z $chosen ]]
	then exit
fi

commit=$(echo -e "Added\nCleaned\nFixed\nTest" | rofi -dmenu -sep "\n" -p "Commit:" )
if [[ -z $commit ]]
	then exit
fi
echo $commit
cd $HOME/mygit/$chosen
git add .
git commit -m $commit
git push git@gitlab.com:k1ake/$chosen.git
notify-send "Changes pushed to:" "$chosen"
read -p "Press Enter to exit"
