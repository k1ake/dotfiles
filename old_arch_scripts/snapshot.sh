#!/bin/bash
if [ ! -d /home/merzost/documents/scripts/tmp/ ]
	then mkdir /home/merzost/documents/scripts/tmp
fi
if [ -e /home/merzost/documents/scripts/tmp/snap.txt ]
	then echo "Snapped already"
else
	touch /home/merzost/documents/scripts/tmp/snap.txt
	echo "Creating snap!!!"
	timeshift-autosnap
	echo "Snap created, you're safe!"
fi
