#!/usr/bin/env python
if __name__ == "__main__":
    with open("tmp.txt") as f:
        default = f.read()

    default = default[1:-2]
    base_arr=default.split("}, {")
    base_arr[0]=base_arr[0][1:]
    base_arr[-1]=base_arr[-1][:-1]

    new_arr=[i.split(", ") for i in base_arr]


    pages = sum([len(i) for i in new_arr])//18

    all_apps = dict()
    for i in new_arr:
        for j in i:
            tmp_arr = j.split(": ", 1)
            all_apps[tmp_arr[0]]=tmp_arr[1]

    # print(*all_apps.keys(), sep="\n")

    dict_to_sort=dict()
    tmp=str()
    for k in all_apps.keys():
        tmp=k
        while ".desktop" in tmp:
            # tmp=tmp[:-(len(".desktop")+0)]
            tmp=tmp.split(".desktop")
            tmp="".join(tmp)
        if "." in tmp:
            # tmp=tmp[len("org.gnome."):]
            tmp=tmp.split(".")
            tmp="-".join(tmp[2:]) if len(tmp) >2 else "".join(tmp[1:])
        
        if "'" in tmp:
            tmp=tmp.split("'")
            tmp="".join(tmp)
        dict_to_sort[tmp]=k

    keys_to_sort = list(dict_to_sort.keys())
    keys_to_sort.sort()
    almost_sorted_dict = {i: dict_to_sort[i] for i in keys_to_sort}

    sorted_dict = dict()
    for v in almost_sorted_dict.values():
        sorted_dict[v]="<{'position': <!@#$%>}>"

    arr=list()
    cur_page=0
    tmp_dict=dict()
    for i,k in enumerate(sorted_dict.keys()):
        if i-18*cur_page>=18:
            cur_page+=1
            arr.append(", ".join([f"{k}: {v}" for k,v in tmp_dict.items()]))
            # arr.append(tmp_dict)
            tmp_dict=dict()
            
        tmp_dict[k]=sorted_dict[k].replace("!@#$%",str(i-18*cur_page))

    answer="[{" +"}, {".join(arr) + "}]"    
    # print(answer, sep="\n\n")

    with open("output.txt","w") as f:
        f.write(answer)
else:
    print("booba")

