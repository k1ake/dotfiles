if [ ! -f "tmp.mic" ]
then
	echo "Normal" > "tmp.mic"
fi

current_preset=$(cat "tmp.mic")

case $current_preset in
	Normal) new_preset="Uber-loud" ;;
	Uber-loud) new_preset="Normal" ;;
esac

if [ $new_preset ]
then
	easyeffects -l $new_preset
	echo $new_preset > "tmp.mic"
	echo "Switched preset from" $current_preset "to" $new_preset
	notify-send "Mic preset" "Switched preset from $current_preset to $new_preset "
else
	echo "error"
fi
