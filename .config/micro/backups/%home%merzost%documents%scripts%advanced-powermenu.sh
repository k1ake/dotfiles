#!/bin/bash
shutdown=""
firmware=""
lock="a"
suspend="鈴"
#"""鈴""廓"ﰬ"""
logout=""

options="$lock\n$firmware\n$logout\n$suspend\n$shutdown"

chosen=$(echo -e $options | rofi -dmenu -theme powermenu.rasi -selected-row 2)
if [[ -z $chosen ]]
	then exit
fi
case $chosen in
	$shutdown) 		poweroff;;
	$firmware) 		systemctl reboot --firmware-setup;;
	$lock) 			$HOME/documents/scripts/lockscreen.sh;;
	$suspend) 		systemctl hibernate;;
	$logout) 		openbox --restart;;
esac
