if status is-interactive
    # Commands to run in interactive sessions can go here
end
function fish_greeting
    macchina
end

alias cdc="cd;clear"
alias ..="cd .."
alias rm="rm -i"
alias mv="mv -i"
alias cp="cp -i"
alias ls='lsd'
alias la='lsd -A --blocks name --blocks size --blocks date --date +%d-%m-%y"|"%H:%M:%S'
alias updatedb='sudo updatedb'
alias r='ranger --choosedir=$HOME/.rangerdir; set LASTDIR $(cat $HOME/.rangerdir); cd $LASTDIR'
alias ssd='sudo nvme smart-log /dev/nvme0'
alias eth='sudo dmesg | grep -E "enp|firmw|dhc"'
alias cu='sh /home/merzost/Scripts/check_nobara_39.sh'
alias minecraft='java -jar /home/merzost/Apps/TLauncher-2.895/TLauncher-2.895.jar && exit'
alias fix_7-1='systemctl --user restart pipewire.socket pipewire-pulse.socket'
alias gpush='git push git@gitlab.com:k1ake/'
