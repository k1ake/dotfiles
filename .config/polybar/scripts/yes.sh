#!/bin/bash
u=$(xprop -name "polybar-yes_DP-2" _NET_WM_PID | grep -o '[[:digit:]]*')
if [ $u -Z ]
	then polybar yes &
	else kill $u
fi
