#!/bin/bash
THEME=sidebar
LOCATION=2
XOFFSET=-10
YOFFSET=30

power=$(echo "Logout|Reboot|Shutdown|Restart openbox" | rofi -sep "|" \
 -dmenu -i -p 'Power Menu: ' "" -hide-scrollbar -font "Ubuntu 12"\
 -theme $THEME -xoffset $XOFFSET -yoffset $YOFFSET -location $LOCATION)
	case "$power" in
		*'Logout') openbox --exit;;
		*'Reboot') systemctl reboot;;
		*'Shutdown')systemctl poweroff;;
		*'Restart openbox') openbox --restart;;
	esac
