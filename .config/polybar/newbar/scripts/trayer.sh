#!/bin/bash
u=$(xprop -name "trayer" _NET_WM_PID | grep -o '[[:digit:]]*')
if [ $u -Z ]
	then trayer --edge bottom \
		--align right --distancefrom top --distance 50 \
		--width 5% --alpha 30 --transparent true \
		--height 23 --tint 0x292524 &
		sleep 7
		u=$(xprop -name "trayer" _NET_WM_PID | grep -o '[[:digit:]]*')
		kill $u
	else kill $u
fi
